//
//  Constant.swift
//  Everave
//
//  Created by kirti on 02/05/19.
//

import Foundation

//https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=sankalp&inputtype=textquery&fields=photos,formatted_address,name,geometry&key=AIzaSyCXvfRRtOmb25pFOGaPHMd0n2TlTAkQc-8



/// ---- Constant Variable----
let kUserID = "kUserID"
let kUserName = "kUserName"
let kUserPhotoUrl = "kUserPhotoUrl"
let klogged =  "klogged2"
let kloginData =  "kloginData"

let colorTabbar = "7B27C9"
let colorsegment = "683DA8"


/// ---- Web Api Name----
let kHostpath = "http://35.162.18.74/api/"
//let kHostpath = "http://192.168.0.30:8000/Everave/api/"
let kLogin = "user/login"
let kRegister = "user/register"
let kCheckValidation = "user/check_exist"
let kgetProfile = "user/get_user_profile"
let kgetFriend = "user/get_friends"
let kuploadProfile = "user/upload_photo"
let keditProfile = "user/edit_user"
let kaddFriends = "user/add_friend"
let kallUsers = "user/get_users"
let ksearchByUsername = "user/search_user_by_name"
let kUserPromote = "user/promote"
let kraveclientToken = "user/get_client_token"


let kpostMedia = "media/post_media"
let kpostText = "media/post_text"
let kgetTextMedia = "media/get_post_text"
let kgetAllTextPOST = "media/get_all_post_text"
let klikeMedia = "media/like_media"
let kgetALLMediaList = "media/get_post_media"
let kgetALLComment = "media/get_comment"
let kaddComment = "media/leave_comment"
let kdeleteComment = "media/delete_comment"
let ksearchByMediaHashtag = "media/search_media_by_hashtag"
//"media/search_media_by_username"
let ksearchByHashTag = "media/search_hashtags"




let kgetMYRaveList = "rave/get_my_raves"
let kgetIgoIdoRaveList = "rave/get_igo_ido_rave"
let kgetRecomTemplate = "rave/get_template"
let kgetFavTemplate = "rave/get_favorite_template"
let kaddtoWishList = "rave/add_wishlist"
let kjoinRave = "rave/join_rave"
let kcreateRave = "rave/create_rave"
let keditRave = "rave/edit_rave"
let kgetRecommendLocation = "rave/get_recommended_location"
let kgetFavoriteLocation = "rave/get_favorite_location"
let kgetPublicRaveList = "rave/get_public_raves"
let kgetWishList = "rave/get_wishlist_rave"
let kgetRaveStatics = "rave/get_statics"
let kraveHide = "rave/allow_hide"
let kraveUnHide = "rave/allow_hide"

let kraveSendPayment = "rave/payment_test"














let laterText = "Coming soon"


//************* Roster_Type ************
let CHAT:Int = 0
//let GROUPCHAT:Int = 1
let BROADCAST:Int = 2
let NORMAL:Int = 3
//************* End ************


// ChatsTable CoreData Idenentity Keys
//Message Types   // 0 - Text | 1 - Image | 2 - Documents | 3 - Location | 4 - Contact | 5 - Video | 6 - Notification

//************* File Type ************
let Type_TEXT =  "Text"
let Type_IMAGE = "Image"
let Type_VIDEO = "video"
let GROUPCHAT =  "Group"
//let BROADCAST:Int = 2
