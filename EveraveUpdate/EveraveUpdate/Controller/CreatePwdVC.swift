//
//  CreatePwdVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/12/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class CreatePwdVC: BaseVC1 {

    @IBOutlet weak var edtPwd: UITextField!
    @IBOutlet weak var edtConfirmPwd: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edtPwd.addPadding(.left(30))
        edtConfirmPwd.addPadding(.left(30))
    }
    @IBAction func gotoMainPage(_ sender: Any) {
        self.gotoMainVC()
    }
    
}
