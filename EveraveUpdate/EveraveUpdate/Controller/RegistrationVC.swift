//
//  RegistrationVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/12/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class RegistrationVC: BaseVC1 {
    
    @IBOutlet weak var edtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        edtEmail.addPadding(.left(30))
    }
    @IBAction func gotoBirth(_ sender: Any) {
        self.gotoNavPresent1("BirthSignVC")
    }
    
}
