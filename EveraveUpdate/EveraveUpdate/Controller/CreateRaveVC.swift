//
//  CreateRaveVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/14/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class CreateRaveVC: BaseVC1 {

    @IBOutlet weak var edtLocation: UITextField!
    @IBOutlet weak var edtDate: UITextField!
    @IBOutlet weak var edtTimeFrom: UITextField!
    @IBOutlet weak var edtTimeTo: UITextField!
    @IBOutlet weak var edtPartyName: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        editInit()
    }
    func editInit()  {
        edtLocation.addPadding(.left(10))
        edtLocation.insertText("234 Monroe Walk")
        edtDate.addPadding(.left(10))
        edtDate.insertText("2019-06-18")
        edtTimeFrom.addPadding(.left(10))
        edtTimeFrom.insertText("11 pm")
        edtTimeTo.addPadding(.left(10))
        edtTimeTo.insertText("6 am")
        edtPartyName.addPadding(.left(0))
        //edtPartyName.insertText("Enter Party name")
        
        edtPartyName.attributedPlaceholder = NSAttributedString(string: "Enter party name",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    @IBAction func closeBtnClicked(_ sender: Any) {
        goBackHome()
    }
    @IBAction func addOptions(_ sender: Any) {
        gotoNavPresent1("AddOptionsVC")
    }
    @IBAction func gotoPreview(_ sender: Any) {
        gotoNavPresent1("PreviewVC")
    }
    @IBAction func goPublish(_ sender: Any) {
        gotoNavPresent1("ProfileMyRaves")
    }
}
