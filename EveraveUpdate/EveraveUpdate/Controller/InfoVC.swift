//
//  SettingsVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class InfoVC : BaseVC1 , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var titleView: UIView!
    
    var infoDatasource = [SettingModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDatasource()

        // Do any additional setup after loading the view.
    }
    
    func initDatasource() {
       
        let infoOption = [R.string.TERMS_OF_USE,R.string.PRIVACY_POLICY,R.string.ABOUT_US ,R.string.RATE_US]
        for i in 0 ..< 4{
            let one = SettingModel (infoOption[i])
            infoDatasource.append(one)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(titleView, letter: R.string.INFO.uppercased() , fontsize: 30, position: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return infoDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.entity = infoDatasource[indexPath.row]
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let aa = indexPath.row
        switch  aa {
        case 0 : // change password
            print(aa)
            break
        case 1 :  // payment Method
//            let toVC =
           print(aa)     //self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC")
//            self.navigationController?.pushViewController(toVC!,animated: true)
            break
        case 2 : // Auto Printer
//            showToast("auto printer")
            print(aa)
            break
        case 3 :  // Choose watermark
//            let toVC =
//                self.storyboard?.instantiateViewController(withIdentifier: "WatermarkVC")
//            self.navigationController?.pushViewController(toVC!,animated: true)
            break
       
        default :
            print(aa)
        }
    }
}
