//
//  MessageViewVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/25/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults

class MessageViewVC: BaseVC1 {

    @IBOutlet weak var topCaptionView: UIView!
    @IBOutlet weak var searchTextfield: UITextField!
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        msgDatasource.removeAll()
        editInit()
        collectionInit()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(topCaptionView, letter: R.string.MESSAGES , fontsize: 30, position: 1)
    }
    
    func editInit()  {
        
        searchTextfield.addPadding(.left(16))
        searchTextfield.attributedPlaceholder = NSAttributedString(string: "Search message",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    func collectionInit() {
        let msgAvatar = ["https://res.cloudinary.com/demo/image/upload/w_400,h_400,c_crop,g_face,r_max/w_200/lady.jpg","https://res.cloudinary.com/demo/image/upload/w_400,h_400,c_crop,g_face,r_max/w_200/lady.jpg","https://res.cloudinary.com/demo/image/upload/w_400,h_400,c_crop,g_face,r_max/w_200/lady.jpg"]
       let msgName = ["Jorgeya","Nelsona","Alina"]
        
       let msgContent = ["Hello, It's me","Is the herval Way The Right Way","Power Ballad yoga is the only way to start a Fusion day!"]
        
       let msgTime = ["Today, 9:30","Today, 8:30","Today, 6:10"]
       let unreadMsgNum = ["2","0","0"]
       let readStatus = [false,true, true]
        
       for i in 0 ..< 3{
           let one = MessageSwipeModel (msgAvatar:msgAvatar[i],msgUserName:msgName[i], msgContent:msgContent[i],unreadMsgNum:unreadMsgNum[i],msgTime:msgTime[i],readStatus : readStatus[i])
           msgDatasource.append(one)
       }
    }
    @IBAction func goBack(_ sender: Any) {
        self.goBack()
    }
    

    @IBAction func addRaveBtn(_ sender: Any) {
        self.gotoNavPresent1("CreateRaveVC")
    }
    @IBAction func gotoHome(_ sender: Any) {
       goBackHome()
    }
    @IBAction func gotoProfile(_ sender: Any) {
         self.gotoNavPresent1("ProfileMyRaves")
    }
}

extension MessageViewVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return msgDatasource.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageSwipeCell", for: indexPath) as! MessageSwipeCell
        
        cell.entity = msgDatasource[indexPath.row]
    
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("clicked")
        msgIndexNum = indexPath.row
        gotoMessageSendVC()
    }
}

extension MessageViewVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let h: CGFloat = 80
        let w = collectionView.frame.size.width
        
        return CGSize(width: w, height: h)
        
    }
    
}

extension MessageViewVC: SwipeCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else {
           return nil
        }
        //let requestId = datasource[indexPath.row].requestFriendId

        let deleteAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
            //self.msgDatasource[indexPath.row].requestFriendName)
            msgDatasource.remove(at: indexPath.row)
            self.ui_collectionView.reloadData()

        }

        let unreadAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in

            msgDatasource[indexPath.row].readStatus = false
            msgDatasource[indexPath.row].unreadMsgNum = "1"
            self.ui_collectionView.reloadData()

        }
               // customize swipe action
        deleteAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
        unreadAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)

               // customize the action appearance
        deleteAction.image = UIImage(named: "delete-1")
        unreadAction.image = UIImage(named: "unread-1")
        deleteAction.backgroundColor = UIColor.init(named: "ColorLightPrimary")
        unreadAction.backgroundColor = UIColor.init(named: "ColorLightPrimary")
        
        deleteAction.title = "delete"
        unreadAction.title = "unread"
        deleteAction.textColor = UIColor.lightGray
        unreadAction.textColor = UIColor.lightGray

        return [deleteAction,unreadAction]

    }
}
