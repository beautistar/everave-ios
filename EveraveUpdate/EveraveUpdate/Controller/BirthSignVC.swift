//
//  BirthSignVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/12/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class BirthSignVC: BaseVC1 {

    @IBOutlet weak var edtBirthday: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        edtBirthday.addPadding(.left(30))
    }

    @IBAction func gotoPwd(_ sender: Any) {
        self.gotoNavPresent1("CreatePwdVC")
    }
}
