//
//  SettingsVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC1 , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var titleView: UIView!
    
    var settingDatasource = [SettingModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDatasource()

        // Do any additional setup after loading the view.
    }
    
    func initDatasource() {
       
        let settingOption = [R.string.ADD_FRIENDS,R.string.NOTIFICATIONS,R.string.LANGUAGE ,R.string.SECRITY,R.string.SUPPORT,R.string.INFO,R.string.BLACK_LIST,R.string.SHARE,R.string.LOGOUT]
        for i in 0 ..< 9{
            let one = SettingModel (settingOption[i])
            settingDatasource.append(one)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(titleView, letter: R.string.SETTINGS , fontsize: 30, position: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return settingDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.entity = settingDatasource[indexPath.row]
        let indextValue = indexPath.row
        
        if(indextValue == 1 ){
            cell.switch_btn.isHidden = false
            
        }
        else{
            cell.switch_btn.isHidden = true
        }
        
        if(indextValue == 2)
        {
            cell.languageCaption.isHidden = false
            cell.languageCaption.text = R.string.ENGLISH
        }
        else{
             cell.languageCaption.isHidden = true
        }
        
        if(indextValue == 1 || indextValue == 7 || indextValue == 8)
        {
            cell.rightArrow.isHidden = true
            
        }
        else{
             cell.rightArrow.isHidden = false
        }
        
        if indextValue == 8 {
            cell.setting_lbl.textColor = UIColor.init(named: "ColorBrown")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let aa = indexPath.row
        switch  aa {
        case 0 :
            break
        case 1 :
            break
        case 2 :
            gotoNavPresent1("LanguageVC")
            break
        case 3 :
            break
        case 4 :
            break
        case 5 :
            break
        case 6 :
            gotoNavPresent1("BlackListVC")
            break
        case 7 :
            break
        case 8 :
            logOut()
            break

        default :
            print(aa)
        }
    }
    @IBAction func backBtnClicked(_ sender: Any) {
        goBack()
    }
}
