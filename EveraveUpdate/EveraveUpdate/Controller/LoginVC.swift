//
//  LoginVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/10/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults


class LoginVC: BaseVC1,ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        editInit()
        navBarHidden()
        self.hideKeyboardWhenTappedAround()
        loadLayout()
    
    }
    
    func editInit() {
         edtEmail.addPadding(.left(30))
         edtPwd.addPadding(.left(30))
    }
    
    func loadLayout() {
         edtEmail.text = "Test@gmail.com"
         edtPwd.text = "123456"
    }
    @IBAction func signInBtnClicked(_ sender: Any) {
        validator.registerField(edtEmail, errorLabel: nil , rules: [RequiredRule(),EmailRule()])
         validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        
         validator.styleTransformers(success:{ (validationRule) -> Void in
             
//             // clear error label
//             validationRule.errorLabel?.isHidden = true
//             validationRule.errorLabel?.text = ""
//
//             if let textField = validationRule.field as? UITextField {
//                 textField.layer.borderColor = UIColor.green.cgColor
//                 textField.layer.borderWidth = 1
//             } else if let textField = validationRule.field as? UITextView {
//                 textField.layer.borderColor = UIColor.green.cgColor
//                 textField.layer.borderWidth = 1
//             }
         }, error:{ (validationError) -> Void in
             print("error")
             validationError.errorLabel?.isHidden = false
             validationError.errorLabel?.text = validationError.errorMessage
             if let textField = validationError.field as? UITextField {
//                 textField.layer.borderColor = UIColor.red.cgColor
//                 textField.layer.borderWidth = 1.0
             } else if let textField = validationError.field as? UITextView {
//                 textField.layer.borderColor = UIColor.red.cgColor
//                 textField.layer.borderWidth = 1.0
             }
         })
         validator.validate(self)
    }
    
    func validationSuccessful() {
           self.validationSuccess()
          }
          
          func validationFailed(_ errors: [(Validatable, ValidationError)]) {
              print("validation error")
          }
       
       func validationSuccess() {
           
//           guard let email = edtEmail.text, let password = edtPwd.text else { return }
               
               self.loginSuccess()
               
//               let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
//               alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//               self.display(alertController: alertController)
          // }

       }
       
       func display(alertController: UIAlertController) {
           self.present(alertController, animated: true, completion: nil)
       }
       
       func loginSuccess() {
        gotoVC("StartSC")
       }
    @IBAction func gotoSignUp(_ sender: Any) {
        gotoNavPresent1("SignupNameVC")
    }
    
}
