//
//  LanguageVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class LanguageVC: BaseVC1 {

    @IBOutlet weak var titleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(titleView, letter: R.string.LANGUAGE.uppercased() , fontsize: 30, position: 1)
    }
    
    @IBAction func goBack(_ sender: Any) {
        goBack()
    }
}
