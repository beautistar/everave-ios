//
//  DetailOptionVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/15/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class DetailOptionVC: BaseVC1 ,UITextFieldDelegate {

    @IBOutlet weak var underline: UIView!
    
    @IBOutlet weak var edtMusic: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edtMusic.delegate = self
        
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        makeBlockEdit()
//        print("endediting")
//
//    }
//
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        makeBlockEdit()
        print("shouldendediting")
        edtMusic.text = ""
        return true;
    }
    
    func makeBlockEdit()  {
        
        var overlayButton = UIButton(type: .custom)
        overlayButton.setTitle(edtMusic.text, for: .normal)
        overlayButton.backgroundColor = UIColor.gray
        overlayButton.cornerRadius = 8
        
//        overlayButton.addTarget(self, action: #selector(displayBookmarks(_:)), for: .touchUpInside)
        //overlayButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)


        // Assign the overlay button to a stored text field
        self.edtMusic.leftView = overlayButton
        self.edtMusic.leftViewMode = .always
        
        //self.edtMusic.addSubview(overlayButton)
        self.edtMusic.didAddSubview(overlayButton)
    }
    @IBAction func goPublish(_ sender: Any) {
        gotoNavPresent1("ProfileMyRaves")
    }
    
}
