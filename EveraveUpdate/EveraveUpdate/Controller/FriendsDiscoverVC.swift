//
//  FriendsDiscoverVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/24/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class FriendsDiscoverVC: BaseVC1 {

    @IBOutlet weak var userName: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(userName, letter: "@party _girl", fontsize: 20, position: 1)
       
    }
    //============= main Tab ==============//
    
    @IBAction func addRaveBtn(_ sender: Any) {
        self.gotoNavPresent1("CreateRaveVC")
    }
    @IBAction func gotoHome(_ sender: Any) {
       goBackHome()
    }
    @IBAction func gotoProfile(_ sender: Any) {
         return
    }
    
    //============= top tab ==============//
    
    @IBAction func gotoTickets(_ sender: Any) {
        self.gotoNavPresent1("TicketsVC")
    }
    @IBAction func gotoCalendar(_ sender: Any) {
        self.gotoNavPresent1("CalendarVC")
    }
    
    @IBAction func gotoFriend(_ sender: Any) {
        return
    }
    
    //============= sub tab ==============//
    @IBAction func subDiscover(_ sender: Any) {
        return
    }
    
    @IBAction func subFriends(_ sender: Any) {
        self.gotoNavPresent1("FriendsRequestVC")
    }
    
    
    @IBAction func gotoSetting(_ sender: Any) {
        goSetting()
    }
    
    //// this is the dummy treatment
    @IBAction func menuBtnClicked(_ sender: Any) {
        
    }
    
    @IBAction func gotoMessageView(_ sender: Any) {
        gotoMessageViewVC()
    }
}

