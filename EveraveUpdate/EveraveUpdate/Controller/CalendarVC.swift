//
//  CalendarVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import VACalendar

class CalendarVC: BaseVC1 {
    
    @IBOutlet weak var topImageView: UIView!
    ///===========================//
    @IBOutlet weak var monthHeaderView: VAMonthHeaderView! {
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "LLLL"
            
            let appereance = VAMonthHeaderViewAppearance(
                previousButtonImage: #imageLiteral(resourceName: "previous"),
                nextButtonImage: #imageLiteral(resourceName: "next"),
                dateFormatter: dateFormatter
            )
            monthHeaderView.delegate = self
            monthHeaderView.appearance = appereance
            monthHeaderView.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .veryShort, calendar: defaultCalendar)
            weekDaysView.appearance = appereance
        }
    }
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar
    }()
    
    var calendarView: VACalendarView!
    
    //===================//

    @IBOutlet weak var editBtnView: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let calendar = VACalendar(calendar: defaultCalendar)
        calendarView = VACalendarView(frame: .zero, calendar: calendar)
         calendarView.showDaysOut = false
         calendarView.selectionStyle = .single
        calendarView.tintColor = .white
         calendarView.monthDelegate = monthHeaderView
         calendarView.dayViewAppearanceDelegate = self
         calendarView.monthViewAppearanceDelegate = self
         calendarView.calendarDelegate = self
         calendarView.scrollDirection = .horizontal
         calendarView.setSupplementaries([
             
             (Date().addingTimeInterval(-(60 * 60 * 70)), [VADaySupplementary.bottomDots([.red, .magenta])]),
             (Date().addingTimeInterval((60 * 60 * 110)), [VADaySupplementary.bottomDots([.red])]),
             (Date().addingTimeInterval((60 * 60 * 370)), [VADaySupplementary.bottomDots([.blue, .darkGray])]),
             (Date().addingTimeInterval((60 * 60 * 430)), [VADaySupplementary.bottomDots([.orange, .purple, .cyan])])
             ])
         
         // add marker for today
        
         //currnetday = VADay(date: Date(),state: .selected, calendar: defaultCalendar)
         
         view.addSubview(calendarView)
    }
    
       override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           createGradientLabel(editBtnView, letter: "@party _girl", fontsize: 20, position: 1)
       }
    
    @IBOutlet weak var totalview: UIView!
    @IBOutlet weak var viewPosition: UIView!
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if calendarView.frame == .zero {
            
            let y = monthHeaderView.bounds.maxY  + weekDaysView.bounds.maxY + viewPosition.bounds.maxY + totalview.frame.height/11
            calendarView.frame = CGRect(
                x: 0,
                y: y,
                width: view.frame.width,
                height: view.frame.height/3
            )
            calendarView.setup()
        }
    }

    //============= main Tab ==============//
       
       @IBAction func addRaveBtn(_ sender: Any) {
           self.gotoNavPresent1("AddOptionsVC")
       }
       @IBAction func gotoHome(_ sender: Any) {
          goBackHome()
       }
       @IBAction func gotoProfile(_ sender: Any) {
            return
       }
    
    //============= top tab ==============//
    
    @IBAction func gotoTickets(_ sender: Any) {
        self.gotoNavPresent1("TicketsVC")
    }
    @IBAction func gotoCalendar(_ sender: Any) {
        return
    }
    @IBAction func gotoFriend(_ sender: Any) {
        self.gotoNavPresent1("FriendsDiscoverVC")
    }
    @IBAction func gotoSetting(_ sender: Any) {
        goSetting()
    }
    
    @IBAction func gotoMessageView(_ sender: Any) {
        gotoMessageViewVC()
    }
}

extension CalendarVC: VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView.nextMonth()
    }
    
    func didTapPreviousMonth() {
        calendarView.previousMonth()
    }
    
}

extension CalendarVC: VAMonthViewAppearanceDelegate {
    
    func leftInset() -> CGFloat {
        return 10.0
    }
    
    func rightInset() -> CGFloat {
        return 10.0
    }
    
    func verticalMonthTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    func verticalMonthTitleColor() -> UIColor {
        return .black
    }
    
    func verticalCurrentMonthTitleColor() -> UIColor {
        return .red
    }
    
}

extension CalendarVC: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 214 / 255, green: 214 / 255, blue: 219 / 255, alpha: 1.0)
        case .selected:
            return .black
        case .unavailable:
            return .lightGray
        default:
            return .white
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            print("background red set")
            return .red
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .circle
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -7
        }
    }
    
}

extension CalendarVC: VACalendarViewDelegate {
   
    func selectedDate(_ date: Date) {
        print(date)
 
    }
    
}
