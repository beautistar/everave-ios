//
//  StatusVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/17/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class StatusVC: BaseVC1 {
    
    @IBOutlet weak var topView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(topView, letter: R.string.STATUS, fontsize: 30, position: 1)
        
    }
    @IBAction func goBack(_ sender: Any) {
        goBack()
    }
    
}
