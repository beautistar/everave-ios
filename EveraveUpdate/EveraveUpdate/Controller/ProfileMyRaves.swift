//
//  ProfileMyRaves.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/16/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class ProfileMyRaves: BaseVC1 {
    
    @IBOutlet weak var visitedNum: UIView!
    @IBOutlet weak var organizedNum: UIView!
    @IBOutlet weak var friendsNum: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        createGradientLabel(visitedNum, letter: "153", fontsize: 34, position: 0)
        createGradientLabel(organizedNum, letter: "123", fontsize: 34, position: 0)
        createGradientLabel(friendsNum, letter: "75", fontsize: 34, position: 0)
    }
    
    @IBAction func addRaveBtn(_ sender: Any) {
        self.gotoNavPresent1("CreateRaveVC")
    }
    @IBAction func gotoHome(_ sender: Any) {
       goBackHome()
    }
    @IBAction func gotoProfile(_ sender: Any) {
         self.gotoNavPresent1("TicketsVC")
    }
    
    
    @IBAction func gotoTickets(_ sender: Any) {
        return
    }
    @IBAction func gotoCalendar(_ sender: Any) {
         self.gotoNavPresent1("CalendarVC")
    }
    @IBAction func gotoFriend(_ sender: Any) {
        //print("gotoFriend")
        self.gotoNavPresent1("FriendsDiscoverVC")
    }
    
    @IBAction func subTickets(_ sender: Any) {
        self.gotoNavPresent1("TicketsVC")
    }
   
    @IBAction func subMyRave(_ sender: Any) {
        return
    }
    
    @IBAction func subFavorite(_ sender: Any) {
        print("Favorite")
        self.gotoNavPresent1("FavoriteVC")
    }
    @IBAction func gotoSetting(_ sender: Any) {
        self.goSetting()
    }
    
    @IBAction func goMyRaveShow(_ sender: Any) {
        gotoNavPresent1("MyRaveShowVC")
    }
    
    @IBAction func gotoMessageView(_ sender: Any) {
        gotoMessageViewVC()
    }
    
}
