//
//  MainVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/14/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import GoogleMaps




class MainVC: BaseVC1 {
    
     private let locationManager = CLLocationManager()
     @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
        
        do {
          // Set the map style by passing the URL of the local file.
          if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
            mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
          } else {
            NSLog("Unable to find style.json")
          }
        } catch {
          NSLog("One or more of the map styles failed to load. \(error)")
        }
        
    }
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
           
           // 1
           let geocoder = GMSGeocoder()
           //self.addressLabel.unlock()
           
           // 1
           
           //let labelHeight = self.addressLabel.intrinsicContentSize.height
           //self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                               //bottom: labelHeight, right: 0)


           
           // 2
           geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
               guard let address = response?.firstResult(), let lines = address.lines else {
                   return
               }
               
               // 3
               //self.addressLabel.text = lines.joined(separator: "\n")
               
             
               // 1
              //let labelHeight = self.addressLabel.intrinsicContentSize.height
               //self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                   //bottom: labelHeight, right: 0)
               
                   UIView.animate(withDuration: 0.25) {
                       //2
//                      self.imageviewbottomConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                      self.view.layoutIfNeeded()
                   }

           }
       }
    
    @IBAction func addRaveBtn(_ sender: Any) {
        self.gotoNavPresent1("CreateRaveVC")
    }
    @IBAction func gotoHome(_ sender: Any) {
       return
    }
    @IBAction func gotoProfile(_ sender: Any) {
         self.gotoNavPresent1("TicketsVC")
    }
}

// MARK: - CLLocationManagerDelegate
//1
extension MainVC: CLLocationManagerDelegate {
    // 2
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        // 3
        guard status == .authorizedWhenInUse else {
            return
        }
        // 4
        locationManager.startUpdatingLocation()
        
        //5
        mapView.isMyLocationEnabled = true
        //mapView.settings.myLocationButton = true
    }
    
    // 6
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        // 7
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        
        // 8
        locationManager.stopUpdatingLocation()
    }
}

// MARK: - GMSMapViewDelegate
extension MainVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //addressLabel.lock()
    }

}
