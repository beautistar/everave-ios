//
//  MessageSendVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/26/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON
import SwiftyUserDefaults

class MessageSendVC: BaseVC1, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var avartarImg: UIImageView!
    @IBOutlet weak var searchTextFiled: UITextField!
    
    @IBOutlet weak var msgTextField: UITextField!
    
    var msgdataSource =  [ChatModel]()
    @IBOutlet weak var tbvChat: UITableView?
    let cellSpacingHeight: CGFloat = 0 // cell line spacing
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
        dataSourceInit()
        
        tbvChat?.estimatedRowHeight = 60
        //tbvChat?.rowHeight = UITableView.automaticDimension
        self.tbvChat?.separatorStyle = .none
        
    }
    
    func dataSourceInit() {
        
        let me = [false,true, true]
         
        let msgContent = ["Hello, It's me","Ok!","hi!"]
         
        let msgTime = ["Today, 9:30","Today, 8:30","Today, 6:10"]
        
        let readStatus = [false,true, true]
         
        for i in 0 ..< 3{
            let one = ChatModel ( me : me[i],timestamp : msgTime[i], msgContent : msgContent[i], readState : readStatus[i])
            msgdataSource.append(one)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        createGradientLabel(titleView, letter: msgDatasource[msgIndexNum].msgUserName , fontsize: 30, position: 1)
        
        let url = URL(string: msgDatasource[msgIndexNum].msgAvatar)
        avartarImg.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
        
    }
    
    func editInit()  {
           searchTextFiled.addPadding(.left(16))
           searchTextFiled.attributedPlaceholder = NSAttributedString(string: "Search message",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        msgTextField.addPadding(.left(16))
        msgTextField.attributedPlaceholder = NSAttributedString(string: "Your message",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.goBack()
    }
    @IBAction func sendAction(_ sender: Any) {
        if checkValid() {
            doSend()
           
        }
    }
    
    func checkValid() -> Bool {
        
        self.view.endEditing(true)
        
        if msgTextField.text!.isEmpty {
            return false
        }
        
        return true
    }
    
    func doSend() {
        
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        var chatObject = [String: String]()
        
        let one = ChatModel ( me : true,timestamp : "now", msgContent :msgTextField.text! , readState : false)
        msgdataSource.append(one)
        tbvChat?.reloadData()
        msgTextField.text = ""
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgdataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tbvChat?.dequeueReusableCell(withIdentifier: "ChatCell", for:indexPath) as! ChatCell
        cell.entity = msgdataSource[indexPath.row]
        
//        let backgroundView = UIView()
//        backgroundView.backgroundColor = UIColor.clear
//        cell.selectedBackgroundView = backgroundView
        return cell
    }
    
//    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
}





