//
//  SignupNameVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/12/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class SignupNameVC: BaseVC1 {

    @IBOutlet weak var edtsignName: UITextField!
    @IBOutlet weak var edtSignPwd: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        edtsignName.addPadding(.left(30))
        edtSignPwd.addPadding(.left(30))
    }
    @IBAction func gotoRegis(_ sender: Any) {
        gotoNavPresent1("RegistrationVC")
    }
    

}
