//
//  AddOptionsVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/15/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class AddOptionsVC: BaseVC1 {
    
    var desOption = false
    var musicOption = false
    var mediaOption = false
    var privacyOption = false
    var particiOption = false
    var priceOption = false
    
    
    @IBOutlet weak var des1: UIImageView!
    @IBOutlet weak var des2: UIImageView!
    
    @IBOutlet weak var musicOption1: UIImageView!
    @IBOutlet weak var musicOption2: UIImageView!
    
    @IBOutlet weak var mediaOption1: UIImageView!
    @IBOutlet weak var mediaOption2: UIImageView!
    
    @IBOutlet weak var privacyOption1: UIImageView!
    @IBOutlet weak var privacyOption2: UIImageView!
    
    @IBOutlet weak var particiOption1: UIImageView!
    @IBOutlet weak var particiOption2: UIImageView!
    
    @IBOutlet weak var priceOption1: UIImageView!
    @IBOutlet weak var priceOption2: UIImageView!
    
    @IBOutlet weak var topView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        optionImageShow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(topView, letter:R.string.ADD_OPTIONS , fontsize: 30, position: 1)
    }
    @IBAction func gotoBack(_ sender: Any) {
        goBack()
        
    }
    @IBAction func addBtnClicked(_ sender: Any) {
        gotoNavPresent1("DetailOptionVC")
    }
    
    func optionImageShow() {
        if desOption{
            des1.isHidden = false
            des2.isHidden = true
        }
        else if !desOption{
            des1.isHidden = true
            des2.isHidden = false
        }
        
        if musicOption{
            musicOption1.isHidden = false
            musicOption2.isHidden = true
        }
        else if !musicOption{
            musicOption1.isHidden = true
            musicOption2.isHidden = false
        }
        
        if mediaOption{
            mediaOption1.isHidden = false
            mediaOption2.isHidden = true
        }
        else if !mediaOption{
            mediaOption1.isHidden = true
            mediaOption2.isHidden = false
        }
        
        if privacyOption{
            privacyOption1.isHidden = false
            privacyOption2.isHidden = true
        }
        else if !privacyOption{
            privacyOption1.isHidden = true
            privacyOption2.isHidden = false
        }
        
        if particiOption{
            particiOption1.isHidden = false
            particiOption2.isHidden = true
        }
        else if !particiOption{
            particiOption1.isHidden = true
            particiOption2.isHidden = false
        }
        
        if priceOption{
            priceOption1.isHidden = false
            priceOption2.isHidden = true
        }
        else if !priceOption{
            priceOption1.isHidden = true
            priceOption2.isHidden = false
        }
    }
    
    @IBAction func description(_ sender: Any) {
        desOption = !desOption
        optionImageShow()
    }
    @IBAction func musicGenres(_ sender: Any) {
        musicOption = !musicOption
        optionImageShow()
    }
    @IBAction func addMedia(_ sender: Any) {
        mediaOption = !mediaOption
        optionImageShow()
    }
    @IBAction func privacy(_ sender: Any) {
        privacyOption = !privacyOption
        optionImageShow()
    }
    @IBAction func partifician(_ sender: Any) {
        particiOption = !particiOption
        optionImageShow()
    }
    @IBAction func price(_ sender: Any) {
        priceOption = !priceOption
        optionImageShow()
    }
}
