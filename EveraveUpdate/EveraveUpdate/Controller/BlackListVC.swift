//
//  SettingsVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class BlackListVC: BaseVC1 , UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var titleView: UIView!
    
    var BlackListDatasource = [BlackListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDatasource()

        // Do any additional setup after loading the view.
    }
    
    func initDatasource() {
        
        let userAvatar = ["https://res.cloudinary.com/demo/image/upload/w_400,h_400,c_crop,g_face,r_max/w_200/lady.jpg","https://res.cloudinary.com/demo/image/upload/w_400,h_400,c_crop,g_face,r_max/w_200/lady.jpg","https://res.cloudinary.com/demo/image/upload/w_400,h_400,c_crop,g_face,r_max/w_200/lady.jpg"]
        let userName = ["Jorgeya","Nelsona","Alina"]
        for i in 0 ..< 3{
            let one = BlackListModel (userAvatar[i], userName: userName[i])
            BlackListDatasource.append(one)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createGradientLabel(titleView, letter: R.string.BLACK_LIST , fontsize: 30, position: 1)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return BlackListDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlackListCell") as! BlackListCell
        cell.entity = BlackListDatasource[indexPath.row]
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    @IBAction func goBack(_ sender: Any) {
        goBack()
    }
}
