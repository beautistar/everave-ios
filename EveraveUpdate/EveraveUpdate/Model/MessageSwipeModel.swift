//
//  MessageSwipeModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/25/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class MessageSwipeModel{
    
    var msgAvatar = ""
    var msgUserName = ""
    var msgContent = ""
    var unreadMsgNum = ""
    var msgTime = ""
    var readStatus : Bool?
    
    init(msgAvatar:String,msgUserName:String, msgContent:String,unreadMsgNum:String,msgTime:String,readStatus : Bool){
        
        self.msgAvatar = msgAvatar
        self.msgUserName = msgUserName
        self.msgContent = msgContent
        self.msgTime = msgTime
        self.unreadMsgNum = unreadMsgNum
        self.readStatus = readStatus
        
    }
    
//    init(dict:JSON){
//        self.requestFriendId = dict[PARAMS.REQUEST_ID].stringValue
//        self.requestFriendName = dict[PARAMS.USER][PARAMS.FIRSTNAME].stringValue + " " + dict[PARAMS.USER][PARAMS.FAMILYNAME].stringValue
//        self.requestFriendEmail = dict[PARAMS.USER][PARAMS.EMAIL].stringValue
//        self.requestFriendPhoto = HOST +  dict[PARAMS.USER][PARAMS.PROFILEPICTURE].stringValue
//    }
}
