//
//  ChatModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/26/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class ChatModel {
    
    var me : Bool
    var readState : Bool
    var timestamp : String
    var msgContent : String
    init(me : Bool,timestamp : String, msgContent : String, readState : Bool) {
        
        self.timestamp = timestamp
        self.msgContent = msgContent
        self.me = me
        self.readState = readState
    }
      
}
