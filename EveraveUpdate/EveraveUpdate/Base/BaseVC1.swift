//
//  BaseVC1.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/21/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
//import Toast_Swift
//import SVProgressHUD
//import SwiftyUserDefaults
//import SwiftyJSON

class BaseVC1: UIViewController {
    
    var gradientLayer: CAGradientLayer!
    var mainVC : MainVC!
    var loginVC : LoginVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func createGradientLabel(_ targetView : UIView, letter : String,fontsize : Int ,position : Int) {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.yellow.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // Create a label and add it as a subview
        let label = UILabel(frame: targetView.bounds)
        label.text = letter
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(fontsize))
        
        if position == 0 {
            label.textAlignment = .center
        }
        else if position == 1{
            label.textAlignment = .left
        }
        
        else if position == 2{
            label.textAlignment = .right
        }
        
        targetView.addSubview(label)
        
        // Tha magic! Set the label as the views mask
        targetView.mask = label
    }
    
    
//    func showMessage1(_ message : String) {
//        self.view.makeToast(message)
//    }
    
    func gotoMainVC(){
          let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
          mainVC = (storyBoard.instantiateViewController(withIdentifier: "MainVC") as! MainVC)
          mainNav = NavigationController(rootViewController: mainVC)
          mainNav.modalPresentationStyle = .fullScreen
          self.present(mainNav, animated: true, completion: nil)
          mainNav.isNavigationBarHidden = true
    }
    
    func logOut(){
          let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
          loginVC = (storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC)
          loginNav = NavigationController(rootViewController: loginVC)
          loginNav.modalPresentationStyle = .fullScreen
          self.present(loginNav, animated: true, completion: nil)
          loginNav.isNavigationBarHidden = true
    }
    
    func gotoVC(_ nameVC: String){
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: nameVC)
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!, animated: false, completion: nil)
    }
    
    func gotoNavPresent1(_ storyname : String) {
          let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
          toVC?.modalPresentationStyle = .fullScreen
          self.navigationController?.pushViewController(toVC!, animated: false)
            
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func goBackHome() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func goSetting() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
    
    func gotoMessageSendVC() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageSendVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
    
    func gotoMessageViewVC() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageViewVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
       
   func navBarHidden() {
       self.navigationController?.isNavigationBarHidden = true
   }
    
   func navBarTransparent1() { self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
      self.navigationController?.navigationBar.shadowImage = UIImage()
      self.navigationController?.navigationBar.isTranslucent = true
      self.navigationController?.view.backgroundColor = .clear
    }
    
    func navBarwithColor() { self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.view.backgroundColor = .blue
   }
    
//       func showHUD1() {
//           SVProgressHUD.show();
//       }
//
//       func showHUDWithTitle1(title: String) {
//           SVProgressHUD.show(withStatus: title)
//       }
//
//       func hideHUD1() {
//           SVProgressHUD.dismiss()
//       }
    
}
extension UITextField {
    enum PaddingSide1 {
        
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }
    
    func addPadding1(_ padding: PaddingSide1) {
        self.leftViewMode = .always
        self.layer.masksToBounds = true
        
        switch padding {
            
        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always
            
        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always
            
        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.leftViewMode = .always
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}

extension UIView {
    func dropShadowleft1(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.8
        layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        layer.shadowRadius = 3.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func ShadowCenter1(scale: Bool = true) {
           layer.masksToBounds = false
           layer.shadowColor = UIColor.white.cgColor
           layer.shadowOpacity = 0.8
           layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
           layer.shadowRadius = 3.0
           layer.shouldRasterize = true
           layer.rasterizationScale = scale ? UIScreen.main.scale : 1
       }
    
    func dropShadowbottom1(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.white.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize(width: 0.0, height:4.0)
        layer.shadowRadius = 2.0
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
}

extension UIViewController {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)

        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }

        var color: UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask

        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0

        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format:"#%06x", rgb)
    }
}

extension UIButton {
    func configure(color: UIColor = .blue, font: UIFont = UIFont.boldSystemFont(ofSize: 12)) {
        self.setTitleColor(color, for: .normal)
        self.titleLabel?.font = font
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
    }

    func configure(icon: UIImage, color: UIColor? = nil) {
        self.setImage(icon, for: .normal)
        if let color = color {
            tintColor = color
        }
    }

    func configure(color: UIColor = .blue,
                   font: UIFont = UIFont.boldSystemFont(ofSize: 12),
                   cornerRadius: CGFloat,
                   borderColor: UIColor? = nil,
                   backgroundColor: UIColor,
                   borderWidth: CGFloat? = nil) {
        self.setTitleColor(color, for: .normal)
        self.titleLabel?.font = font
        self.backgroundColor = backgroundColor
        if let borderColor = borderColor {
            self.layer.borderColor = borderColor.cgColor
        }
        if let borderWidth = borderWidth {
            self.layer.borderWidth = borderWidth
        }
        self.layer.cornerRadius = cornerRadius
    }
}

extension UITextField {
    func configure(color: UIColor = .blue,
                   font: UIFont = UIFont.boldSystemFont(ofSize: 12),
                   cornerRadius: CGFloat,
                   borderColor: UIColor? = nil,
                   backgroundColor: UIColor,
                   borderWidth: CGFloat? = nil) {
        if let borderWidth = borderWidth {
            self.layer.borderWidth = borderWidth
        }
        if let borderColor = borderColor {
            self.layer.borderColor = borderColor.cgColor
        }
        self.layer.cornerRadius = cornerRadius
        self.font = font
        self.textColor = color
        self.backgroundColor = backgroundColor
    }
}



