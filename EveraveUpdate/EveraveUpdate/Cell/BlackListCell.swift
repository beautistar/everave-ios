//
//  SettingCell.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class BlackListCell: UITableViewCell {
    
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userAvartar: UIImageView!
    
    
    var entity : BlackListModel!{
        
        didSet{
            userName.text = entity.userName
            
            let url = URL(string: entity.avatarImg)
            userAvartar.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
