//
//  IntroCell.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/12/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit

class IntroCell: UICollectionViewCell {
    
    @IBOutlet weak var img_back: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_top: UILabel!
    @IBOutlet weak var lbl_bottom: UILabel!
    @IBOutlet weak var btnCaption: UILabel!
    
    var entity : IntroModel! {
        
        didSet{
            
            if let img = UIImage(named: entity.imgBack) {
                img_back.image = img
            }
            lbl_title.text = entity.title
            lbl_top.text = entity.lbltop
            lbl_bottom.text = entity.lblbottom
            btnCaption.text = entity.lblbtn
            
        }
    }
    
}

