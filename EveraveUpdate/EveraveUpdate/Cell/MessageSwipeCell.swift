//
//  MessageSwipeCell.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/25/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//
import Foundation
import UIKit
import SwipeCellKit
import Kingfisher

class MessageSwipeCell: SwipeCollectionViewCell {
    
    @IBOutlet var msgAvatar: UIImageView!
    @IBOutlet var msgUserName: UILabel!
    @IBOutlet var msgContent: UILabel!
    @IBOutlet var msgTime: UILabel!
    @IBOutlet var msgUnreadNum: UILabel!
    
    @IBOutlet var msgReadTick: UIImageView!
    
    @IBOutlet weak var UnreadView: UIView!
    
     
    var entity:MessageSwipeModel!{
        
        didSet{
            let url = URL(string: entity.msgAvatar)
            msgAvatar.kf.setImage(with: url,placeholder: UIImage(named: "account_photo"))
            msgUserName.text = entity.msgUserName
            msgContent.text = entity.msgContent
            msgTime.text = entity.msgTime
            msgUnreadNum.text = entity.unreadMsgNum
            
            if let state = entity.readStatus{
                msgReadTick.isHidden = !state
                UnreadView.isHidden = state
                msgUnreadNum.isHidden = state
            }
            
        }
    }
    
}
