//
//  ChatCell.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/26/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import Kingfisher

class ChatCell: UITableViewCell {

    
    @IBOutlet weak var meView: UIView!
    @IBOutlet weak var melblContent: UILabel!
    @IBOutlet weak var melblTime: UILabel!
    @IBOutlet weak var meback: UIImageView!
    @IBOutlet weak var mecorner: UIImageView!
    
    @IBOutlet weak var youView: UIView!
    @IBOutlet weak var youlblContent: UILabel!
    @IBOutlet weak var youlblTime: UILabel!
    @IBOutlet weak var youreadStatue: UIImageView!
    @IBOutlet weak var youback: UIImageView!
    @IBOutlet weak var yourcorner: UIImageView!
    
    var entity : ChatModel!{
        didSet {
            
            if entity.me {
                youView.isHidden = true
                youlblContent.isHidden = true
                youlblTime.isHidden = true
                youreadStatue.isHidden = true
                youback.isHidden = true
                yourcorner.isHidden = true
                
                melblContent.text = entity.msgContent
                melblTime.text = entity.timestamp
                meView.isHidden = false
                meback.isHidden = false
                mecorner.isHidden = false
               
            }
            else if !entity.me  {
               melblContent.isHidden = true
               melblTime.isHidden = true
               meView.isHidden = true
               meback.isHidden = true
               mecorner.isHidden = true
                
                youView.isHidden = false
                youlblContent.text = entity.msgContent
                youlblTime.text = entity.timestamp
                youreadStatue.isHidden = entity.readState
                youback.isHidden = false
                yourcorner.isHidden = false
            
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
